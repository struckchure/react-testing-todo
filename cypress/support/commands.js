// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import "cypress-testing-library/add-commands";
import * as faker from "faker";
import { userBuilder, todoBuilder } from "./generate";

Cypress.Commands.add("createUser", (overrides = {}) => {
  const user = userBuilder(overrides);
  return cy.request("POST", "/api/auth/register", user).then(result => user);
});

Cypress.Commands.add("loginNewUser", (overrides = {}) => {
  const user = userBuilder(overrides);
  return cy.request("POST", "/api/auth/register", user).then(resp => {
    window.localStorage.setItem("auth", resp.body.token);
    return user;
  });
});

Cypress.Commands.add("createTodo", (overrides = {}) => {
  const todo = todoBuilder(overrides);
  return cy
    .request({
      method: "POST",
      url: "/api/todos",
      headers: {
        Authorization: `Bearer ${window.localStorage.getItem("auth")}`
      },
      body: todo
    })
    .then(resp => resp.body);
});
