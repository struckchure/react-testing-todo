import { IUserCreate } from './../register';
import { Application, NextFunction, Request, Response, Router } from 'express';
import passport from 'passport';
import { registerJWTAuth } from './jwtStrategy';
import { User } from '../../entity';
import { login } from '../login';
import { ILogin } from '../../types/types';
import { register } from '../register';

function resolveUser(req: Request, resolve: Function, reject: Function, strategy: string) {
    return (err: Error, user: User) => {
        if (user) {
            req.user = user;
            resolve(user);
        }
        reject(new Error('no User'));
    };
}

const NOOP = (...args: any[]) => {};

export const authRouter = () => {
    const router = Router();

    router.post('/api/auth/login', async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { email, password } = req.body;
            const loginObject: ILogin = {
                email,
                password,
            };
            const { token, user } = await login(loginObject);
            res.send({ token, user });
        } catch (e) {
            next(e);
        }
    });

    router.post('/api/auth/register', async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { email, password, firstName, lastName } = req.body;
            const userToCreate: IUserCreate = {
                email,
                password,
                firstName,
                lastName,
            };
            const { token, user } = await register(userToCreate);
            res.send({ token, user });
        } catch (e) {
            next(e);
        }
    });
    return router;
};

export function createAuthentication(app: Application) {
    passport.serializeUser((user: User | { user: User; token: string }, done: Function) => {
        if (user instanceof User) {
            return done(null, user.id);
        }

        done(null, user.user.id);
    });

    passport.deserializeUser(async (id: string, done: Function) => {
        const user = await User.findOne(id);
        done(null, user);
    });

    registerJWTAuth();

    return async function authentication(req: Request, res: Response, next: NextFunction) {
        try {
            await new Promise((resolve, reject) => {
                passport.authenticate(
                    'jwt',
                    { session: true, failWithError: true },
                    resolveUser(req, resolve, reject, 'jwt'),
                )(req, res, NOOP);
            });
            next();
        } catch (e) {
            next(e);
        }
    };
}
