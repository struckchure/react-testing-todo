const Joi = require('joi');
const app = require('express')();
const validator = require('express-joi-validation')({});

export const createTodoBodySchema = Joi.object({
    title: Joi.string().required(),
    done: Joi.boolean(),
});

export const updateTodoBodySchema = Joi.object({
    title: Joi.string(),
    done: Joi.boolean(),
});

export const idTodoParamSchema = Joi.object({
    id: Joi.string().required(),
});
