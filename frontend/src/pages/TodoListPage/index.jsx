// @flow

import * as React from 'react'

import { connect } from 'react-redux'

import * as TodoActions from '../../store/actions/Todo'
import { TodoList } from '../../components/TodoList'
import { CreateTodoInput } from '../../components/CreateTodoInput/index'
import { ITodo } from '../../types'

// The Redux Component behave exactly like in simple Redux

type Props = {
  todos: Object[],
  getTodos: () => void,
  createTodoItem: (title: string) => void,
  updateTodoItem: (id: string, todo: Object) => void,
  removeTodoItem: (id: string) => void,
}

class ReduxTodoList extends React.Component<Props> {
  componentDidMount() {
    this.props.getTodos()
  }

  render() {
    const { todos, createTodoItem, updateTodoItem, removeTodoItem } = this.props
    return (
      <>
        <CreateTodoInput createTodoItem={createTodoItem} />
        <TodoList
          todos={todos}
          removeTodoItem={removeTodoItem}
          updateTodoItem={updateTodoItem}
        />
      </>
    )
  }
}

const mapStateToProps = state => ({
  todos: state.todos,
})

const mapDispatchToProps = dispatch => ({
  getTodos: () => {
    dispatch(TodoActions.getTodos())
  },
  createTodoItem: title => {
    dispatch(TodoActions.createTodoItem(title))
  },
  updateTodoItem: (todoId: string, todo: ITodo) => {
    dispatch(TodoActions.updateTodoItem(todoId, todo))
  },
  removeTodoItem: todoId => {
    dispatch(TodoActions.removeTodoItem(todoId))
  },
})

export const TodoListPage = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ReduxTodoList)
