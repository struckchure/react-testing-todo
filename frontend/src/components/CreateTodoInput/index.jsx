// @flow

import React from 'react'
import { Input } from 'antd'

type Props = {
  createTodoItem: (todoTitle: string) => void,
}
type State = {
  value: string,
}

export class CreateTodoInput extends React.Component<Props, State> {
  static initialState = {
    value: '',
  }

  state = CreateTodoInput.initialState

  changeValue = (e: SyntheticEvent<HTMLInputElement>) => {
    this.setState({ value: e.target.value })
  }

  handlePressEnterNewTodo = e => {
    const { createTodoItem } = this.props
    createTodoItem(e.target.value)
    this.setState(CreateTodoInput.initialState)
  }

  render() {
    const { value } = this.state
    return (
      <Input
        id="create-todo"
        value={value}
        onChange={this.changeValue}
        placeholder="What needs to be done?"
        onPressEnter={this.handlePressEnterNewTodo}
      />
    )
  }
}
