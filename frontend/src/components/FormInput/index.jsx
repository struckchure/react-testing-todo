// @flow

import React from 'react'
import { Form, Input } from 'antd'

type Props = {
  values: Object,
  errors: Object,
  handleSubmit: (e: any) => void,
  setFieldValue: (field: string, value: string) => void,
  setFieldTouched: (field: string) => void,
  name: string,
  placeholder: string,
  type: string,
  label: string,
}
export const FormInput = ({
  values,
  errors,
  handleSubmit,
  setFieldValue,
  setFieldTouched,
  name,
  placeholder,
  label,
  type,
}: Props) => (
  <Form.Item
    hasFeedback={!!errors[name]}
    validateStatus={errors[name] && 'error'}
    help={errors[name]}
    label={label}
  >
    <Input
      type={type}
      placeholder={placeholder}
      value={values[name]}
      onChange={event => setFieldValue(name, event.currentTarget.value)}
      onBlur={() => setFieldTouched(name)}
      onPressEnter={handleSubmit}
    />
  </Form.Item>
)
