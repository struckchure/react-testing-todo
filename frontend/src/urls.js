// @flow

export const urls = {
  api: {
    auth: {
      login: '/api/auth/login',
      register: '/api/auth/register',
    },
    todos: '/api/todos',
    todo: (id: string) => `/api/todos/${id}`,
  },
  app: {
    home: '/',
    auth: {
      login: '/auth/login',
      register: '/auth/register',
    },
  },
}
