#!/usr/bin/env sh

set -ex

docker pull $IMAGE_TAG_APP:$CI_COMMIT_REF_SLUG || true
docker pull $IMAGE_TAG_APP:develop || true
docker pull $IMAGE_TAG_DATABASE:$CI_COMMIT_REF_SLUG || true
docker pull $IMAGE_TAG_DATABASE:develop || true

docker build \
    --cache-from $IMAGE_TAG_APP:$CI_COMMIT_REF_SLUG \
    --cache-from $IMAGE_TAG_APP:develop \
    -t $IMAGE_TAG_APP:$CI_COMMIT_REF_SLUG \
    -f .docker/api/Dockerfile \
    api

docker build \
    --cache-from $IMAGE_TAG_DATABASE:$CI_COMMIT_REF_SLUG \
    --cache-from $IMAGE_TAG_DATABASE:develop \
    -t $IMAGE_TAG_DATABASE:$CI_COMMIT_REF_SLUG \
    -f .docker/postgres/Dockerfile \
    api

docker push $IMAGE_TAG_APP:$CI_COMMIT_REF_SLUG
docker push $IMAGE_TAG_DATABASE:$CI_COMMIT_REF_SLUG
